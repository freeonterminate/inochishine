﻿(*
 * Form Utils
 *
 * PLATFORMS
 *   macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/04/08 Version 1.0.0
 * 2018/05/13 Version 1.0.1  Rename Show -> BringToFront
 * 2018/05/21 Version 1.0.2  BringToFront overloaded (Only Windows)
 * 2019/11/27 Version 1.0.3  macOS Catalina support
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.Form.Mac;

{$IFNDEF OSX}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

interface

implementation

uses
  System.Classes
  , System.SysUtils
  , System.UITypes
  , Macapi.CocoaTypes
  , Macapi.ObjectiveC
  , Macapi.AppKit
  , Macapi.Foundation
  , Macapi.Helpers
  , Macapi.ObjCRuntime
  , FMX.Helpers.Mac
  , FMX.Forms
  , FMX.Platform
  , FMX.Platform.Mac
  , PK.Utils.Form
  ;

type
  TFormUtilsMac = class(TInterfacedObject, IFormUtils)
  private class var
    FMethodAdded: Boolean;
  private var
    FNSApp: NSApplication;
    FRegsitedForm: TCommonCustomForm;
    FRegistedProc: TFormStateNotifyProc;
  public
    constructor Create; reintroduce;
    function FindWindow(const iCaption: String): NativeInt;
    procedure HideTaskbar;
    procedure ShowTaskbar;
    procedure HideMainForm;
    procedure BringToFront; overload;
    procedure BringToFront(const iWnd: NativeUInt); overload;
    procedure RegistMinimizeProc(
      const iForm: TCommonCustomForm;
      const iProc: TFormStateNotifyProc);
    function GetAutoRestore: Boolean;
  end;

  TFormUtilsFactoryMac = class(TFormUtilsFactory)
  public
    function CreateFormUtils: IFormUtils; override;
  end;

{ TFormUtilsFactoryMac }

function TFormUtilsFactoryMac.CreateFormUtils: IFormUtils;
begin
  Result := TFormUtilsMac.Create;
end;

procedure RegisterFormUtilsMac;
begin
  TPlatformServices.Current.AddPlatformService(
    IFormUtilsFactory,
    TFormUtilsFactoryMac.Create);
end;

{ Utils }

function CheckValidMainForm: Boolean;
begin
  Result :=
    (not Application.Terminated) and
    (Application.MainForm <> nil) and
    (not (csDestroying in Application.MainForm.ComponentState))
end;

procedure ShowMainWindow(const iNSApp: NSApplication);
begin
  if iNSApp.mainWindow <> nil then
  begin
    iNSApp.activateIgnoringOtherApps(True);
    iNSApp.mainWindow.makeKeyAndOrderFront(nil);
  end;

  if CheckValidMainForm then
    Application.MainForm.Show;
end;

{ NSApplicationDelegate }

var
  GAppDelegateDummy: NSObject;

function applicationShouldHandleReopen(
  self: ID; 
  _cmd: SEL; 
  application: Pointer; 
  hasVisibleWindows: Boolean): Boolean; cdecl;
begin
  var NSApp := TNSApplication.Wrap(TNSApplication.OCClass.sharedApplication);

  ShowMainWindow(NSApp);
  Result := True;
end;

{ TFormUtilsMac }

procedure TFormUtilsMac.BringToFront;
begin
  ShowMainWindow(FNSApp);
end;

procedure TFormUtilsMac.BringToFront(const iWnd: NativeUInt);
begin
  BringToFront;
end;

constructor TFormUtilsMac.Create;
begin
  inherited Create;

  FNSApp := TNSApplication.Wrap(TNSApplication.OCClass.sharedApplication);

  if not FMethodAdded then // 何個も登録されないように
  begin
    if FNSApp.delegate = nil then
    begin
      GAppDelegateDummy := TNSObject.Create;
      FNSApp.setDelegate(NSApplicationDelegate(GAppDelegateDummy));
    end;
    
    var ID := (FNSApp.delegate as ILocalObject).GetObjectID;
    var Clazz := object_getClass(ID);
    var Sel := sel_getUid('applicationShouldHandleReopen:hasVisibleWindows:');

    if class_getInstanceMethod(Clazz, Sel) = nil then
      FMethodAdded :=
        class_addMethod(Clazz, Sel, @applicationShouldHandleReopen, 'B@:@:@:B:')
        > 0;
  end;
end;

function TFormUtilsMac.FindWindow(const iCaption: String): NativeInt;
var
  Apps: NSArray;
begin
  Apps :=
    TNSRunningApplication
    .OCClass
    .runningApplicationsWithBundleIdentifier(StrToNSStr(iCaption));

  if Apps.count > 0 then
    Result := -1
  else
    Result := 0;
end;

function TFormUtilsMac.GetAutoRestore: Boolean;
begin
  Result := FMethodAdded;
end;

procedure TFormUtilsMac.HideMainForm;
begin
  FNSApp.mainWindow.setCanHide(True);
  FNSApp.hide(nil);

  if CheckValidMainForm then
  begin
    Application.MainForm.Visible := False;
    Application.MainForm.WindowState := TWindowState.wsMinimized;
  end;
end;

procedure TFormUtilsMac.HideTaskbar;
begin
  FNSApp.setActivationPolicy(NSApplicationActivationPolicyAccessory);
end;

procedure TFormUtilsMac.RegistMinimizeProc(
  const iForm: TCommonCustomForm;
  const iProc: TFormStateNotifyProc);
begin
  FRegsitedForm := iForm;
  FRegistedProc := iProc;
end;

procedure TFormUtilsMac.ShowTaskbar;
begin
  FNSApp.setActivationPolicy(NSApplicationActivationPolicyRegular);
end;

initialization
  RegisterFormUtilsMac;

end.
