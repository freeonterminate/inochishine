﻿(*
 * PK.Expo.Cell
 *
 * PLATFORMS
 *   Windows 32bit
 *   Windows 64bit
 *   macOS 64bit
 *
 * LICENSE
 *   Copyright (c) 2020 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HISTORY
 *   2020/08/26 Version 1.0.0 First release
 *   2020/08/27 Version 1.1.0 Added animation
 *
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

 unit PK.Expo.Cell;

interface

uses
  System.SysUtils
  , System.Types
  , System.Classes
  , FMX.Ani
  , FMX.Controls
  , FMX.Objects
  ;

type
  TExpoCell = class(TControl)
  private var
    FCellBase: TEllipse;
    FEyeBase: TEllipse;
    FEye: TEllipse;
    FBounceX: TFloatAnimation;
    FBounceY: TFloatAnimation;
    FCenter: TPointF;
    FDA, FDB: Single;
    FRA, FRB: Single;
    FEyeD, FEyeR: Single;
    FIsNeedEye: Boolean;
    FEyeAngle: Single;
  private
    procedure Init;
    procedure BounceXProcessHandler(Sender: TObject);
    procedure BounceYProcessHandler(Sender: TObject);
    procedure SetNeedEye(const Value: Boolean);
    procedure SetEyeAngle(const Value: Single);
  protected
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetEyePos(const AMouseX, AMouseY: Single); overload;
    procedure SetEyePos(const AMousePos: TPointF); overload;
    procedure StartAnimation;
  published
    property EyeAngle: Single read FEyeAngle write SetEyeAngle;
    property IsNeedEye: Boolean read FIsNeedEye write SetNeedEye;
  end;

implementation

uses
  System.UITypes
  , System.Math
  , FMX.Types
  , FMX.Graphics
  ;

{ TExpoCell }

procedure TExpoCell.BounceXProcessHandler(Sender: TObject);
begin
  FCellBase.Position.X := (Width - FCellBase.Width * FCellBase.Scale.X) / 2;
end;

procedure TExpoCell.BounceYProcessHandler(Sender: TObject);
begin
  FCellBase.Position.Y := (Height - FCellBase.Height * FCellBase.Scale.Y) / 2;
end;

constructor TExpoCell.Create(AOwner: TComponent);

  procedure CreateAnimation(
    var ABounce: TFloatAnimation;
    const APropName: String);
  begin
    ABounce := TFloatAnimation.Create(nil);

    ABounce.Parent := FCellBase;

    Abounce.AnimationType := TAnimationType.Out;
    ABounce.Duration := 1;
    Abounce.Interpolation := TInterpolationType.Elastic;
    ABounce.PropertyName := APropName;
    ABounce.StartValue := 0;
    ABounce.StopValue := 1;
  end;

  function CreateEllipse(
    const AParent: TFmxObject;
    const AColor: TAlphaColor): TEllipse;
  begin
    Result := TEllipse.Create(Self);
    Result.Stroke.Kind := TBrushKind.None;
    Result.Fill.Color := AColor;
    Result.HitTest := False;
    Result.Parent := AParent;
  end;

begin
  inherited;

  FCellBase := CreateEllipse(Self, $ffe70010);
  FEyeBase := CreateEllipse(FCellBase, TAlphaColors.White);
  FEye := CreateEllipse(FEyeBase, $ff0166bb);

  CreateAnimation(FBounceX, 'Scale.X');
  CreateAnimation(FBounceY, 'Scale.Y');
  FBounceX.OnProcess := BounceXProcessHandler;
  FBounceY.OnProcess := BounceYProcessHandler;

  FEyeAngle := 45;

  Init;
end;

destructor TExpoCell.Destroy;
begin
  FBounceX.Parent := nil;
  FBounceY.Parent := nil;

  FEye.Free;
  FEyeBase.Free;
  FCellBase.Free;
  FBounceX.Free;
  FBounceY.Free;

  inherited;
end;

procedure TExpoCell.Init;
begin
  FCellBase.SetBounds(0, 0, Width, Height);

  FDA := FCellBase.Width / 2;
  FDB := FCellBase.Height / 2;

  FRA := FDA / 2;
  FRB := FDB / 2;

  FEyeD := FRA;
  if FRA > FRB then
    FEyeD := FRB;

  FEyeR := FEyeD / 2;

  var X := Cos(FEyeAngle) * FDA;
  var Y := Sin(FEyeAngle) * FDB;

  FEyeBase.SetBounds(X, Y, FDA, FDB);
  FEye.SetBounds(0, 0, FEyeD, FEyeD);

  var Pos := FEyeBase.LocalToAbsolute(PointF(0, 0));
  FCenter.X := Pos.X + FRA;
  FCenter.Y := Pos.Y + FRB;

  SetEyePos(0, 0);
end;

procedure TExpoCell.Resize;
begin
  inherited;
  Init;
end;

procedure TExpoCell.SetEyePos(const AMousePos: TPointF);
begin
  SetEyePos(AMousePos.X, AMousePos.Y);
end;

procedure TExpoCell.SetEyeAngle(const Value: Single);
begin
  FEyeAngle := Value;
  InvalidateRect(BoundsRect);
end;

procedure TExpoCell.SetEyePos(const AMouseX, AMouseY: Single);
begin
  var Theta := ArcTan2(AMouseY - FCenter.Y, AMouseX - FCenter.X);
  var X := (FRA - FEyeR) * (Cos(Theta) + 1);
  var Y := (FRB - FEyeR) * (Sin(Theta) + 1);

  FEye.SetBounds(X, Y, FEyeD, FEyeD);
end;

procedure TExpoCell.SetNeedEye(const Value: Boolean);
begin
  FIsNeedEye := Value;

  FEyeBase.Visible := FIsNeedEye;

  InvalidateRect(BoundsRect);
end;

procedure TExpoCell.StartAnimation;
begin
  if FBounceX.Running then
    Exit;

  FBounceX.Start;
  FBounceY.Start;
end;

end.
