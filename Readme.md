﻿# InochiShine

## 概要

いのちの輝きくんのデスクトップマスコットです。

## 開発環境
|             |                     |
|-------------|---------------------|
| Environment | Delphi, RAD Studio  |
| Version     | 10.4.1 Sydney       |
| Framework   | FireMonkey          |
| Support OS  | Windows, macOS      |

## 履歴
2020/08/26  1.00 リリース  
2020/08/27  1.10 MouseDown / Up 時のアニメーション追加  

## Files
| Files                   | Description                          |
|-------------------------|--------------------------------------|
| README.md               | Readme                               |
| InochiShine.dproj       | プロジェクトファイル                 |
| InochiShine.dpr         | プロジェクトソースファイル           |
| uMain.pas               | メインソース                         |
| iMain.fmx               | メインフォーム                       |
| PK.Expo.Cell.pas        | いのちの輝きくんの細胞ソース         |
| PKLib                   | ライブラリフォルダ                   |

## 使い方

InochiShine.dproj を開きます。  
ターゲットを Win 32, Win 64, macOS 64 のどれかにして、ビルドします。  
実行するとデスクトップにいのちの輝きくんがあらわれます。  
  
目がマウスを追いかけてきます（xeyes と同じ）。  
終わらせる場合は、いのちの輝きくんか、タスクトレイのアイコンを右クリックして「コロシテ…コロ…」を選びます。  

## Contact
freeonterminate@gmail.com  
http://twitter.com/pik  
      
# LICENSE
Copyright (c) 2020 HOSOKAWA Jun
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
