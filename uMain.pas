﻿unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Generics.Collections,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Menus,
  PK.TrayIcon, PK.Utils.Form, PK.Expo.Cell, System.ImageList, FMX.ImgList,
  FMX.Objects;

type
  TfrmMain = class(TForm)
    MouseTimer: TTimer;
    menuMain: TPopupMenu;
    menuAbout: TMenuItem;
    menuISep: TMenuItem;
    menuExit: TMenuItem;
    imglstIcon: TImageList;
    Circle1: TCircle;
    Circle2: TCircle;
    Circle3: TCircle;
    Circle4: TCircle;
    Circle5: TCircle;
    Circle6: TCircle;
    Circle7: TCircle;
    Circle8: TCircle;
    Circle9: TCircle;
    Circle10: TCircle;
    Circle11: TCircle;
    Circle12: TCircle;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MouseTimerTimer(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure menuExitClick(Sender: TObject);
    procedure menuAboutClick(Sender: TObject);
  private var
    FTrayIcon: TTrayIcon;
    FFormUtils: TFormUtils;
    FCells: TList<TExpoCell>;
  private
    procedure StartAnimation;
    procedure TrayIconClick(Sender: TObject);
  public
    procedure MouseUp(
      AButton: TMouseButton;
      AShift: TShiftState;
      AX, AY: Single;
      ADoClick: Boolean = True); override;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses
  System.Math
  , PK.Utils.ImageListHelper
  ;

procedure TfrmMain.FormCreate(Sender: TObject);
const
  ICON_NORMAL = 'Normal';
  HINT = 'いのちの輝き';

  function RandSign: Integer;
  begin
    Result := IfThen(Random(2) = 0, +1, -1);
  end;

  procedure CreateCell(const ACircle: TCircle);
  const
    B = 64;
    R = 24;
    D = 16;
    S = 20;
  begin
    var Cell := TExpoCell.Create(Self);
    var Count := FCells.Add(Cell);

    var W := Random(R) + B;
    var H := W + RandSign * Random(D);
    var X := ACircle.Position.X + RandSign * Random(W div S);
    var Y := ACircle.Position.Y + RandSign * Random(H div S);

    Cell.SetBounds(X, Y, W, H);
    Cell.EyeAngle := Random(360);
    Cell.IsNeedEye :=  Odd(Count) and (Random(100) > 15);
    Cell.PopupMenu := menuMain;
    Cell.OnMouseDown := FormMouseDown;

    Cell.Parent := Self;
  end;

begin
  Randomize;

  FFormUtils := TFormUtils.Create;
  FTrayIcon := TTrayIcon.Create;

  var Icon := TBitmap.Create;
  imglstIcon.GetBitmap('Icon', Icon);

  FTrayIcon.AssignPopupMenu(menuMain);
  FTrayIcon.RegisterIcon(ICON_NORMAL, Icon);
  FTrayIcon.ChangeIcon(ICON_NORMAL, HINT);
  FTrayIcon.RegisterOnClick(TrayIconClick);
  FTrayIcon.Apply;

  {$IFDEF MSWINDOWS}
  FFormUtils.HideTaskbar;
  {$ENDIF}

  FCells := TList<TExpoCell>.Create;

  var Circles := [
    Circle1, Circle2, Circle3, Circle4, Circle5, Circle6,
    Circle7, Circle8, Circle9, Circle10, Circle11, Circle12
  ];

  for var i := 0 to 11 do
    CreateCell(Circles[i]);

  for var i := 11 downto 0 do
  begin
    var Cell := FCells[i];
    if Cell.IsNeedEye then
      Cell.BringToFront;
  end;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  for var Cell in FCells do
    Cell.Free;

  FTrayIcon.Free;
  FFormUtils.Free;
end;

procedure TfrmMain.FormMouseDown(
  Sender: TObject;
  Button: TMouseButton;
  Shift: TShiftState;
  X, Y: Single);
begin
  if Button = TMouseButton.mbLeft then
  begin
    StartWindowDrag;
    StartAnimation;
  end;
end;

procedure TfrmMain.menuAboutClick(Sender: TObject);
const
  VERSION =
    'いのちの輝きくん的なもの version 1.10' + sLineBreak +
    'Created by @pik, 2020/08/27.';
begin
  ShowMessage(VERSION);
end;

procedure TfrmMain.menuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.MouseTimerTimer(Sender: TObject);
begin
  var Pos := Screen.MousePos;
  Pos := ScreenToClient(Pos);

  for var Cell in FCells do
    Cell.SetEyePos(Pos);
end;

procedure TfrmMain.MouseUp(
  AButton: TMouseButton;
  AShift: TShiftState;
  AX, AY: Single;
  ADoClick: Boolean);
begin
  inherited;

  StartAnimation;
end;

procedure TfrmMain.StartAnimation;
begin
  for var Cell in FCells do
    Cell.StartAnimation;
end;

procedure TfrmMain.TrayIconClick(Sender: TObject);
begin
  FFormUtils.BringToFront;
end;

end.
